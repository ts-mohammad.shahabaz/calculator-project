import java.util.Scanner;

public class StandardCalculator {
    public void Standard(){
        double value=0;
        Scanner sc = new Scanner(System.in);
        String operator = sc.nextLine();
        for (int i=0;i<operator.length();i++){
            if((operator.charAt(i)) == '/')
            {
                double value1 = Integer.parseInt(String.valueOf(operator.charAt(i-1)))/Integer.parseInt(String.valueOf(operator.charAt(i+1)));
                value+=value1;
                break;
            }
            else if((operator.charAt(i)) == '*')
            {
                int value2 = Integer.parseInt(String.valueOf(operator.charAt(i-1)))*Integer.parseInt(String.valueOf(operator.charAt(i+1)));
                value+=value2;
            }
            else if((operator.charAt(i)) == '+')
            {
                int value3 = Integer.parseInt(String.valueOf(operator.charAt(i-1)))+Integer.parseInt(String.valueOf(operator.charAt(i+1)));
                value+=value3;
            }
            else if((operator.charAt(i)) == '-')
            {
                int value4 = Integer.parseInt(String.valueOf(operator.charAt(i-1)))-Integer.parseInt(String.valueOf(operator.charAt(i+1)));
                value+=value4;
            }
        }
        System.out.println(value);
    }
}