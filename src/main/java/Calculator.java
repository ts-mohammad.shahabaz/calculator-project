import java.util.Scanner;

public class Calculator {
    public static void main(String args[]){

        System.out.println("enter 1 for standard ,2 for scientific");
        Scanner scanner= new Scanner(System.in);
        int sc=scanner.nextInt();
        StandardCalculator calc1 = new StandardCalculator();
        ScientificCalculator calc2 = new ScientificCalculator();
        switch (sc) {
            case 1:
                calc1.Standard();
                break;
            case 2:
                calc2.Scientific();
                calc1.Standard();
                break;
            default:
                System.out.println("Invalid Entry");
        }
    }
}
