import java.util.Scanner;

public class ScientificCalculator {
    public void Scientific(){
        System.out.println("Enter a value to calculate scientific values");
        Scanner scanner=new Scanner(System.in);
        double num = scanner.nextDouble();
        System.out.println("Enter 1 for sin\nEnter 2 for cos\nEnter 3 for tan\nEnter 4 for cot\nEnter 5 for power of a number\nEnter 6 for root of a number:");
        int list = scanner.nextInt();
        switch (list){
            case 1:
                System.out.println(Math.sin(num));
                break;
            case 2:
                System.out.println(Math.cos(num));
                break;
            case 3:
                System.out.println(Math.tan(num));
                break;
            case 4:
                System.out.println(Math.cos(num)/Math.sin(num));
                break;
            case 5:
                System.out.println("Enter exponent value");
                int exponent = scanner.nextInt();
                System.out.println(Math.pow(num,exponent));
                break;
            case 6:
                System.out.println(Math.sqrt(num));
                break;
            default:
                System.out.println("Invalid Entry");

        }
    }
}
